# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [3.0.1 - 2017-12-15]
### Fixed
- Include non-snapshot version of share-dto

## [3.0.0 - 2017-12-15]
### Changed
- Use common namespace as default for QueryResultStatistic

## [2.0.0 - 2017-12-13]
### Changed
- Use common namespace as default for View and Error

## [1.1.1 - 2017-09-13]
### Fixed
- Undo removal of throws declarations

## [1.1.0 - 2017-09-13]
### Added
- Identifier for a QueryResultPage (location + page index)
- Method to check page availablity via HTTP Head

## [1.0.1 - 2017-04-27]
### Added
- Get User Agent Info (ldm name + version info)

## [1.0.0 - 2017-03-30]
### Added
- First implementation
